.PHONY: all list test 



all: list

list:
	@grep '^\.PHONY' Makefile | cut -d' ' -f2- | tr ' ' '\n'

test:
	python -m unittest discover -s unit-tests -p "*_test.py"
