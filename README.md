Basic UAT and Integration tests for NHS projct

## Install

```
npm install .
```

## Run tests

### Cypress tests

```
npm run open
```

### Upload Tests

```
make test
```

## Environment Variables

```
export CYPRESS_API_KEY=<<CKAN API KEY>>
export CYPRESS_BASE_URL=https://ckan.nhs.staging.datopian.com/
```