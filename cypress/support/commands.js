const headers = {'Authorization': Cypress.env('API_KEY')}

Cypress.Commands.add('createDataset', (datasetName, extras={}) => {
  cy.request({
    method: 'POST',
    url: '/api/3/action/package_create',
    headers: headers,
    body: {
      maintainer: 'nhs-user',
      license_id: 'cc-by',
      publisher: 'nhs-user',
      geographic_level: 'global',
      maintainer_email: 'nhs@data.org',
      information_classification: 'public-public-external',
      visibility: 'public',
      data_privacy_regulated: true,
      data_privacy_country: ['ARG'],
      notes: 'empty',
      owner_org: 'uat-testing',
      name: datasetName,
      ...extras
    }
  })
})

Cypress.Commands.add('createTeam', (orgId='uat-testing', extras={}) => {
  cy.request({
    method: 'POST',
    url: '/api/3/action/organization_create',
    headers: headers,
    body: {
      name: orgId,
      ...extras
    }
  })
})

Cypress.Commands.add('purgeTeam', (orgId='uat-testing', extras={}) => {
  cy.request({
    method: 'POST',
    url: '/api/3/action/organization_purge',
    headers: headers,
    body: {
      id: orgId,
      ...extras
    }
  })
})

Cypress.Commands.add('deleteDataset', (datasetName) => {
  cy.request({
      url: '/api/3/action/dataset_purge',
      method: 'POST',
      body: {id: datasetName},
      headers: headers
    })
})

// Performs an XMLHttpRequest instead of a cy.request (able to send data as FormData - multipart/form-data)
Cypress.Commands.add('form_request', (method, url, formData, done) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader('Authorization', Cypress.env('API_KEY'))
    xhr.onload = function () {
        done(xhr);
    };
    xhr.onerror = function () {
        done(xhr);
    };
    xhr.send(formData);
})
