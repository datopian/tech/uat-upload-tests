const headers = {'Authorization': Cypress.env('API_KEY')}

describe('Main Website Pages Are Working', () => {
  it('Main Page', () => {
    cy.visit('/', { headers: headers })
    cy.contains(/search datasets/i)
    cy.contains(/theme/i)
    cy.contains(/dataset/i)
    cy.contains(/about/i)
  })

  it('Dataset Page', () => {
    cy.visit('/dataset', { headers: headers })
  })

  it('Teams Page', () => {
    cy.visit('/organization', { headers: headers })
  })

  it('Groups Page', () => {
    cy.visit('/group', { headers: headers })
  })

  it('About Page', () => {
    cy.visit('/about', { headers: headers })
  })

  it('Support Page', () => {
    cy.visit('/pages/test-internal-page', { headers: headers })
  })
})


describe('Basic GET API Endpoints Are Working', () => {
  it('API: Should redirect to logn when not authorised', () => {
    cy.request({
        url: '/api/3/action/status_show',
        followRedirect: false,
        failOnStatusCode: false
      }).then((resp) => {
        expect(resp.status).to.eq(403)
    })
  })

  it('API: Status Show', () => {
    cy.request({url: '/api/3/action/status_show',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: i18n', () => {
    cy.request({url: '/api/i18n/en', headers: headers }).then((resp) => {
      expect(resp.body[''].lang).to.eq('en')
      expect(resp.body[''].domain).to.eq('ckan')
    })
  })

  it('API: Package List', () => {
    cy.request({url: '/api/3/action/package_list',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: Group List', () => {
    cy.request({url: '/api/3/action/group_list',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: Tag List', () => {
    cy.request({url: '/api/3/action/tag_list',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: Package Search', () => {
    cy.request({url: '/api/3/action/package_search?q=GDX',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: Reasource Search', () => {
    cy.request({url: '/api/3/action/resource_search?query=name:GDX%20Resource',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })

  it('API: Recenty Chnaged Activity List', () => {
    cy.request({url: '/api/3/action/recently_changed_packages_activity_list',  headers: headers }).then((resp) => {
      expect(resp.body.success).to.eq(true)
    })
  })
})


describe('Organization Create/Delete', () => {
  const uuid = Math.ceil(Math.random() * 1000000)
  const orgName = 'uat-test-organization-create' + uuid.toString()

  it('Organization create page', () => {
    cy.visit('/organization/new', {headers: headers})
  })

  it('Create Organization Not-Authorised', () => {
    cy.request({
      url: '/api/3/action/organization_create',
      method: 'POST',
      followRedirect: false,
      failOnStatusCode: false
      }).then((resp) => {
        expect(resp.status).to.eq(403)
    })
  })

  it('Create Organization', () => {
    cy.request({
        url: '/api/3/action/organization_create',
        method: 'POST',
        body: {name: orgName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Organization is there', () => {
    cy.visit('/organization/' + orgName, {headers: headers})
  })

  it('Delete Organization', () => {
    cy.request({
        url: '/api/3/action/organization_delete',
        method: 'POST',
        body: {id: orgName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Purge Organization', () => {
    cy.request({
        url: '/api/3/action/organization_purge',
        method: 'POST',
        body: {id: orgName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })
})

describe('Group Create/Delete', () => {
  const uuid = Math.ceil(Math.random() * 1000000)
  const groupName = 'uat-test-group-create' + uuid.toString()

  it('Group create page', () => {
    cy.visit('/group/new', {headers: headers})
  })

  it('Create Group Not-Authorised', () => {
    cy.request({
      url: '/api/3/action/group_create',
      method: 'POST',
      followRedirect: false,
      failOnStatusCode: false
      }).then((resp) => {
        expect(resp.status).to.eq(403)
    })
  })

  it('Create Group', () => {
    cy.request({
        url: '/api/3/action/group_create',
        method: 'POST',
        body: {name: groupName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Group is there', () => {
    cy.visit('/group/' + groupName, {headers: headers})
  })

  it('Delete Group', () => {
    cy.request({
        url: '/api/3/action/group_delete',
        method: 'POST',
        body: {id: groupName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Purge Group', () => {
    cy.request({
        url: '/api/3/action/group_purge',
        method: 'POST',
        body: {id: groupName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })
})

describe('Dataset Create/Show/Delete', () => {
  const uuid = Math.ceil(Math.random() * 1000000)
  const datasetName = 'uat-test-dataset-create' + uuid.toString()

  before(function () {
    cy.createTeam()
  })

  after(function () {
    cy.purgeTeam()
  })

  it('Dataset create page', () => {
    cy.visit('/dataset/new', {headers: headers})
  })

  it('Create Dataset Not-Authorised', () => {
    cy.request({
      url: '/api/3/action/package_create',
      method: 'POST',
      followRedirect: false,
      failOnStatusCode: false
      }).then((resp) => {
        expect(resp.status).to.eq(403)
    })
  })

  it('Create Dataset', () => {
    cy.request({
        url: '/api/3/action/package_create',
        method: 'POST',
        body: {
          maintainer: 'nhs-user',
          license_id: 'cc-by',
          publisher: 'nhs-user',
          geographic_level: 'global',
          maintainer_email: 'nhs@data.org',
          information_classification: 'public-public-external',
          visibility: 'public',
          data_privacy_regulated: true,
          data_privacy_country: ['ARG'],
          notes: 'empty',
          owner_org: 'uat-testing',
          name: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Dataset is there', () => {
    cy.visit('/dataset/' + datasetName, {headers: headers})
  })

  it('Dataset Show', () => {
    cy.request({
        url: '/api/3/action/package_show',
        method: 'GET',
        body: {
            id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Dataset Delete', () => {
    cy.request({
        url: '/api/3/action/package_delete',
        method: 'POST',
        body: {id: datasetName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('Dataset Purge', () => {
    cy.request({
        url: '/api/3/action/dataset_purge',
        method: 'POST',
        body: {id: datasetName},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })
})

describe('Show Resource Create/Delete', () => {
  const uuid = Math.ceil(Math.random() * 1000000)
  const datasetName = 'uat-test-resource-create' + uuid.toString()
  let resourceID = {}

  before(function () {
    cy.createTeam()
    cy.createDataset(datasetName)
  })

  after(function () {
    cy.deleteDataset(datasetName)
    cy.purgeTeam()
  })


  it('Resource Create Page', () => {
    cy.visit('/dataset/new_resource/' + datasetName, {headers: headers})
  })

  it('Create Resource Not-Authorised', () => {
    cy.request({
      url: '/api/3/action/resource_create',
      method: 'POST',
      followRedirect: false,
      failOnStatusCode: false
      }).then((resp) => {
        expect(resp.status).to.eq(403)
    })
  })


  it('CSV Resource Create', () => {
    cy.request({
        url: '/api/3/action/resource_create',
        method: 'POST',
        body: {
          upload: 'fixtures/csv/100kb.csv',
          package_id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        resourceID.csv = resp.body.result.id
    })
  })

  it('CSV Resource is there', () => {
    cy.visit('/dataset/' + datasetName + '/resource/' + resourceID.csv, {headers: headers})
  })

  it('CSV Resource Show', () => {
    cy.request({
        url: '/api/3/action/resource_show',
        method: 'GET',
        body: {
            id: resourceID.csv,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('CSV Resource Download', () => {
    cy.request({
      url: '/dataset/' + datasetName + '/resource/' + resourceID.csv + '/download/' + '100kb.csv',
      method: 'HEAD'
    }).then((resp) => {
      expect(resp.status).to.eq(200)
    })
  })

  it('CSV Resource Delete', () => {
    cy.request({
        url: '/api/3/action/resource_delete',
        method: 'POST',
        body: {id: resourceID.csv},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('JSON Resource Create', () => {
    cy.request({
        url: '/api/3/action/resource_create',
        method: 'POST',
        body: {
          upload: 'fixtures/json/sample.json',
          package_id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        resourceID.json = resp.body.result.id
    })
  })

  it('JSON Resource is there', () => {
    cy.visit('/dataset/' + datasetName + '/resource/' + resourceID.json, {headers: headers})
  })

  it('JSON Resource Show', () => {
    cy.request({
        url: '/api/3/action/resource_show',
        method: 'GET',
        body: {
            id: resourceID.json,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('JSON Resource Download', () => {
    cy.request({
      url: '/dataset/' + datasetName + '/resource/' + resourceID.json + '/download/' + 'sample.json',
      method: 'HEAD'
    }).then((resp) => {
      expect(resp.status).to.eq(200)
    })
  })

  it('JSON Resource Delete', () => {
    cy.request({
        url: '/api/3/action/resource_delete',
        method: 'POST',
        body: {id: resourceID.json},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('ODS Resource Create', () => {
    cy.request({
        url: '/api/3/action/resource_create',
        method: 'POST',
        body: {
          upload: 'fixtures/ods/sample.ods',
          package_id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        resourceID.ods = resp.body.result.id
    })
  })

  it('ODS Resource is there', () => {
    cy.visit('/dataset/' + datasetName + '/resource/' + resourceID.ods, {headers: headers})
  })

  it('ODS Resource Show', () => {
    cy.request({
        url: '/api/3/action/resource_show',
        method: 'GET',
        body: {
            id: resourceID.ods,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('ODS Resource Download', () => {
    cy.request({
      url: '/dataset/' + datasetName + '/resource/' + resourceID.ods + '/download/' + 'sample.ods',
      method: 'HEAD'
    }).then((resp) => {
      expect(resp.status).to.eq(200)
    })
  })

  it('ODS Resource Delete', () => {
    cy.request({
        url: '/api/3/action/resource_delete',
        method: 'POST',
        body: {id: resourceID.ods},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('PDF Resource Create', () => {
    cy.request({
        url: '/api/3/action/resource_create',
        method: 'POST',
        body: {
          upload: 'fixtures/pdf/sample.pdf',
          package_id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        resourceID.pdf = resp.body.result.id
    })
  })

  it('PDF Resource is there', () => {
    cy.visit('/dataset/' + datasetName + '/resource/' + resourceID.pdf, {headers: headers})
  })

  it('PDF Resource Show', () => {
    cy.request({
        url: '/api/3/action/resource_show',
        method: 'GET',
        body: {
            id: resourceID.pdf,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('PDF Resource Download', () => {
    cy.request({
      url: '/dataset/' + datasetName + '/resource/' + resourceID.pdf + '/download/' + 'sample.pdf',
      method: 'HEAD'
    }).then((resp) => {
      expect(resp.status).to.eq(200)
    })
  })

  it('PDF Resource Delete', () => {
    cy.request({
        url: '/api/3/action/resource_delete',
        method: 'POST',
        body: {id: resourceID.pdf},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('TXT Resource Create', () => {
    cy.request({
        url: '/api/3/action/resource_create',
        method: 'POST',
        body: {
          upload: 'fixtures/txt/sample.txt',
          package_id: datasetName,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        resourceID.txt = resp.body.result.id
    })
  })

  it('TXT Resource is there', () => {
    cy.visit('/dataset/' + datasetName + '/resource/' + resourceID.txt, {headers: headers})
  })

  it('TXT Resource Show', () => {
    cy.request({
        url: '/api/3/action/resource_show',
        method: 'GET',
        body: {
            id: resourceID.txt,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })

  it('TXT Resource Download', () => {
    cy.request({
      url: '/dataset/' + datasetName + '/resource/' + resourceID.txt + '/download/' + 'sample.txt',
      method: 'HEAD'
    }).then((resp) => {
      expect(resp.status).to.eq(200)
    })
  })

  it('TXT Resource Delete', () => {
    cy.request({
        url: '/api/3/action/resource_delete',
        method: 'POST',
        body: {id: resourceID.txt},
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
    })
  })
})

describe('Search', () => {
  const uuid = Math.ceil(Math.random() * 1000000).toString()
  const datasetName = 'uat-test-dataset-create' + uuid

  before(function () {
    cy.createTeam()
    cy.createDataset(datasetName, {
      visibility: 'restricted',
      tags: [{'name': 'unicorn'}]
    })
  })

  after(function () {
    cy.deleteDataset(datasetName)
    cy.purgeTeam()
  })

  it('Search Restricted Dataset', () => {
    cy.visit('dataset?q=' + uuid, {headers: headers})
    cy.contains(/1 dataset found/i)
  })

  it('Search Dataset By Tag', () => {
    cy.visit('dataset?q=unicorn', {headers: headers})
    cy.contains(/1 dataset found/i)
  })


  it('Sort Datasets', () => {
    cy.createDataset('dq-t4041-sort-test-a')
    cy.createDataset('dq-t4041-sort-test-b')
    cy.createDataset('dq-t4041-sort-test-c')
    cy.createDataset('dq-t4041-sort-test-d')

    cy.visit('dataset', {headers: headers})
    cy.get('select[id="field-order-by"]').select('metadata_modified desc')
    cy.get('.dataset-item').first().get('a[href*="dq-t4041-sort-test-d"]')

    cy.request({
        url: '/api/3/action/package_show',
        method: 'GET',
        body: {
            id: 'dq-t4041-sort-test-b',
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
        let result = resp.body.result
        result.notes = 'full'
        cy.request({
          url: '/api/3/action/package_update',
          method: 'POST',
          body: result,
          headers: headers
        })
    })

    cy.visit('dataset', {headers: headers})
    cy.get('select[id="field-order-by"]').select('metadata_modified desc')
    cy.get('.dataset-item').first().get('a[href*="dq-t4041-sort-test-b"]')

    cy.deleteDataset('dq-t4041-sort-test-a')
    cy.deleteDataset('dq-t4041-sort-test-b')
    cy.deleteDataset('dq-t4041-sort-test-c')
    cy.deleteDataset('dq-t4041-sort-test-d')
  })

})

describe('Testing All Existing Resources', () => {

  let dataset_names = []
  let resources = []

  before(function () {
    cy.request({
      url: 'api/3/action/current_package_list_with_resources',
      method: 'GET',
      failOnStatusCode: false
    }).then((resp) => {
      expect(resp.body.success).to.eq(true)
      let datasets = resp.body.result
      for (var i = 0, len=datasets.length; i < len; i++) {
        dataset_names.push(datasets[i].name)
        resources = resources.concat(datasets[i].resources)
      }
      
    })
  })

  it('Datastore Search - All Existing Resources', () => {

    for (var i = 0, len = resources.length; i < len; i++) {
      cy.request({
        url: 'api/3/action/datastore_search',
        method: 'GET',
        failOnStatusCode: false,
        body: {
          resource_id: resources[i].name,
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
      })
    }
  })

  it('Datastore SQL Search - All Existing Resources', () => {

    for (var i = 0, len = resources.length; i < len; i++) {
      cy.request({
        url: 'api/3/action/datastore_search_sql',
        method: 'GET',
        failOnStatusCode: false,
        body: {
          sql : 'SELECT * from ' + resources[i].name + ' limit 5',
        },
        headers: headers
      }).then((resp) => {
        expect(resp.body.success).to.eq(true)
      })
    }
  })

  it('UI Download - All Existing Resources', () => {
    for (var i = 0, len = dataset_names.length; i < len; i++) {
      cy.visit('/dataset/' + dataset_names[i])
      
      //get and iterate over all of the resources links
      cy.get('.dropdown-menu li a').each(function($el, index, $list){
        cy.wrap($el).should('have.attr', 'href')
          .then(function(href){
            //test if the resource link is working
            cy.request('HEAD', href)
          })
      })
    }
    
  })

})
