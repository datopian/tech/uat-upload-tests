import os
import requests
import unittest
import uuid
from urllib.parse import urljoin

API_KEY = os.environ.get('CYPRESS_API_KEY', '73404c4b-67e6-4657-a60e-6e0640ee34ea')
BASE_URL = os.environ.get('CYPRESS_BASE_URL', 'https://ckan.nhs.staging.datopian.com/')


class UploadTest(unittest.TestCase):

    DATASET_ID = str(uuid.uuid4())

    @classmethod
    def setUpClass(cls):
        create_team()
        create_datasaet(cls.DATASET_ID)

    def test__csv_resource_upload_download(self):
        file = open('cypress/fixtures/csv/100kb.csv', 'rb')
        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_create'),
            data = {
              'package_id': self.DATASET_ID,
            },
            headers = {'Authorization': API_KEY},
            files = [('upload', file)]
        )
        file.close()

        self.assertTrue(resp.json().get('success'))
        res_id = resp.json()['result']['id']
        resp = requests.get(
            urljoin(BASE_URL, '/dataset/' + self.DATASET_ID + '/resource/' + res_id),
            headers = {'Authorization': API_KEY}
        )
       

        self.assertEqual(resp.status_code, 200)

        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_delete'),
            data = {
              'id': res_id,
            },
            headers = {'Authorization': API_KEY}
        )
        self.assertTrue(resp.json().get('success'))

    def test__1mb_csv_resource_upload_download(self):
        file = open('cypress/fixtures/csv/1mb.csv', 'rb')
        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_create'),
            data = {
              'package_id': self.DATASET_ID,
            },
            headers = {'Authorization': API_KEY},
            files = [('upload', file)]
        )
        file.close()

        self.assertTrue(resp.json().get('success'))
        res_id = resp.json()['result']['id']
        resp = requests.get(
            urljoin(BASE_URL, '/dataset/' + self.DATASET_ID + '/resource/' + res_id),
            headers = {'Authorization': API_KEY}
        )

        self.assertEqual(resp.status_code, 200)

        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_delete'),
            data = {
              'id': res_id,
            },
            headers = {'Authorization': API_KEY}
        )
        self.assertTrue(resp.json().get('success'))

    def test__10mb_csv_resource_upload_download(self):
        file = open('cypress/fixtures/csv/10mb.csv', 'rb')
        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_create'),
            data = {
              'package_id': self.DATASET_ID,
            },
            headers = {'Authorization': API_KEY},
            files = [('upload', file)]
        )
        file.close()

        self.assertTrue(resp.json().get('success'))
        res_id = resp.json()['result']['id']
        resp = requests.get(
            urljoin(BASE_URL, '/dataset/' + self.DATASET_ID + '/resource/' + res_id ),
            headers = {'Authorization': API_KEY}
        )
        self.assertEqual(resp.status_code, 200)

        resp = requests.post(
            urljoin(BASE_URL, '/api/3/action/resource_delete'),
            data = {
              'id': res_id,
            },
            headers = {'Authorization': API_KEY}
        )
        self.assertTrue(resp.json().get('success'))

    @classmethod
    def tearDownClass(cls):
        purge_dataset(cls.DATASET_ID)
        purge_team()
    

def create_datasaet(dataset_name, extras={}):
    data = {
      'maintainer': 'nhs-user',
      'license_id': 'cc-by',
      'publisher': 'nhs-user',
      'geographic_level': 'global',
      'maintainer_email': 'nhs@data.org',
      'information_classification': 'public-public-external',
      'visibility': 'public',
      'data_privacy_regulated': True,
      'data_privacy_country': ['ARG'],
      'notes': 'empty',
      'owner_org': 'uat-testing',
      'name': dataset_name
    }
    data.update(extras)
    resp = requests.post(
        urljoin(BASE_URL, '/api/3/action/package_create'),
        data=data,
        headers={'Authorization': API_KEY},
    )
    assert resp.json().get('success'), 'Failed To Upload Dataset'

def create_team(org_id='uat-testing'):
    data = {
      'name': org_id
    }
    resp = requests.post(
        urljoin(BASE_URL, '/api/3/action/organization_create'),
        data=data,
        headers={'Authorization': API_KEY},
    )
    assert resp.json().get('success'), 'Failed To Create Team'
    return resp.json().get('result')['id']

def purge_team(org_id='uat-testing'):
    data = {
      'id': org_id
    }
    resp = requests.post(
        urljoin(BASE_URL, '/api/3/action/organization_purge'),
        data=data,
        headers={'Authorization': API_KEY},
    )
    assert resp.json().get('success'), 'Failed To Purge Team'

def purge_dataset(dataset_id):
    resp = requests.post(
        urljoin(BASE_URL, '/api/3/action/dataset_purge'),
        data={'id': dataset_id},
        headers={'Authorization': API_KEY},
    )
    assert resp.json().get('success'), 'Failed To Delete Dataset'

if __name__ == '__main__':
    unittest.main()

